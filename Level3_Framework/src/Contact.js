import React, { Component } from 'react';

export default class Contact extends Component {

    render() {
        return (
          <div className="contact-form">
            <Form />
          </div>
        );
      }
}

class Form extends Component {
  constructor() {
    super();
    this.state = {
      errors: [],
    };
  }

  validate(name, email) {
    const errors = [];
  
    if (name.length === 0) {
      errors.push("Name can't be empty");
    }
  
    if (email.length < 5) {
      errors.push("Email should be at least 5 charcters long");
    }
    if (email.split('').filter(x => x === '@').length !== 1) {
      errors.push("Email should contain a @");
    }
    if (email.indexOf('.') === -1) {
      errors.push("Email should contain at least one dot");
    }
  
    return errors;
  }

  submit = () => {
    const data = {
      name: this.text.value,
      email: this.email.value,
      comment: this.textarea.value,
    }
    const errors = this.validate(data.name, data.email)
    if(errors.length > 0) {
      this.setState({ errors });
      alert(errors)
      return;
    }
    fetch('https://formula-test-api.herokuapp.com/contact', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    })
    .then( success =>{
      alert((JSON.stringify('Message send!')))
    });
  }

  render() {
    return (
      <div className="Form">
        <div className="row">
          <div className="col-md-4 col-md-offset-4">
            <input type="text" className="form-control" placeholder="Name" ref={(input) => this.text = input } />
            <input type="email" className="form-control" placeholder="E-mail" ref={(input) => this.email = input } />
            <textarea className="form-control" rows="5" id="comment" placeholder="Comment" ref={(textarea) => this.textarea = textarea }></textarea>
            <button className="btn btn-default" onClick={this.submit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
}


/* class Submitted extends Component {
    render() {
      return (
        <div className="Submitted">
          <h3 className="text-center text-success">Message send!</h3>
        </div>
      );
    }
  } */