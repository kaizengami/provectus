import React, { Component } from 'react';

export default class Footer extends Component {

    render() {
        return (
            <footer>
                <div className="row">
                    <div className="col-md-8 text-left">® 2016 Dirty Dogs all rights reserved</div>
                    <div className="col-md-4 position">274 Marconi Blvd. Columbus, Ohio 43215 | 614.538.0095 | <a href="">Contact Us</a></div>
                </div>
            </footer>
        );
      }
}
