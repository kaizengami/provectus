// item example
// {
//   name: 'Name', 
//   description: 'Description', 
//   expirationDate: '01-30-1999' 
// }

const filterByExpiration = (items) => {
  // change this function to return only current items
  // where expirationDate > today's date
let today = new Date();
let i = items.length
while (i--) {  
  //converting data format 
  let itemDate = items[i].expirationDate.split('-');
  let itemDateEdit = itemDate.slice(0, 2);
  itemDateEdit.unshift(itemDate[2]);
  itemDate = itemDateEdit.join('-');
  let newItemDate = new Date(itemDate)
  //removing all items older than today's date
  if (today > newItemDate) { 
    items.splice(i, 1);
  } 
}

  console.log(items);
  return items;
};